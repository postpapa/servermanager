﻿$WshShell = New-Object -comObject WScript.Shell
If(!(test-path $PSScriptRoot\Scripts\Configs))
{
    mkdir $PSScriptRoot\Scripts\Configs
}
If(!(test-path $PSScriptRoot\Scripts\Logs))
{
    mkdir $PSScriptRoot\Scripts\Logs
}
If(!(test-path $PSScriptRoot\Scripts\Logs\DONTDELETE\))
{
    mkdir $PSScriptRoot\Scripts\Logs\DONTDELETE
}
If(!(test-path $PSScriptRoot\TaskPlanner))
{
    mkdir $PSScriptRoot\TaskPlanner
}
Clear-Host

Function CreateShortcuts ($server)
{
$Shortcut = $WshShell.CreateShortcut("$PSScriptRoot\TaskPlanner\start-ALL.lnk")
$Shortcut.TargetPath = """C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"""
$Shortcut.Arguments = "-command $PSScriptRoot\Scripts\Start.ps1 $server"
$Shortcut.WorkingDirectory = "$PSScriptRoot\Scripts"
$Shortcut.Save()

$Shortcut = $WshShell.CreateShortcut("$PSScriptRoot\TaskPlanner\restart-ALL.lnk")
$Shortcut.TargetPath = """C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"""
$Shortcut.Arguments = "-command $PSScriptRoot\Scripts\Restart.ps1 $server"
$Shortcut.WorkingDirectory = "$PSScriptRoot\Scripts"
$Shortcut.Save()

$Shortcut = $WshShell.CreateShortcut("$PSScriptRoot\TaskPlanner\stop-ALL.lnk")
$Shortcut.TargetPath = """C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"""
$Shortcut.Arguments = "-command $PSScriptRoot\Scripts\Stop.ps1 $server"
$Shortcut.WorkingDirectory = "$PSScriptRoot\Scripts"
$Shortcut.Save()

$Shortcut = $WshShell.CreateShortcut("$PSScriptRoot\TaskPlanner\update-ALL.lnk")
$Shortcut.TargetPath = """C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"""
$Shortcut.Arguments = "-command $PSScriptRoot\Scripts\Update.ps1 $server"
$Shortcut.WorkingDirectory = "$PSScriptRoot\Scripts"
$Shortcut.Save()

$Shortcut = $WshShell.CreateShortcut("$PSScriptRoot\TaskPlanner\Saveworld-ALL.lnk")
$Shortcut.TargetPath = """C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"""
$Shortcut.Arguments = "-command $PSScriptRoot\Scripts\Saveworld.ps1 $server"
$Shortcut.WorkingDirectory = "$PSScriptRoot\Scripts"
$Shortcut.Save()

$Shortcut = $WshShell.CreateShortcut("$PSScriptRoot\TaskPlanner\Backup-ALL.lnk")
$Shortcut.TargetPath = """C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"""
$Shortcut.Arguments = "-command $PSScriptRoot\Scripts\Backup.ps1 $server"
$Shortcut.WorkingDirectory = "$PSScriptRoot\Scripts"
$Shortcut.Save()

$Shortcut = $WshShell.CreateShortcut("$PSScriptRoot\TaskPlanner\WildDinoWipe-ALL.lnk")
$Shortcut.TargetPath = """C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"""
$Shortcut.Arguments = "-command $PSScriptRoot\Scripts\Dinowipe.ps1 $server"
$Shortcut.WorkingDirectory = "$PSScriptRoot\Scripts"
$Shortcut.Save()

$Shortcut = $WshShell.CreateShortcut("$PSScriptRoot\Manager.lnk")
$Shortcut.TargetPath = """C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"""
$Shortcut.Arguments = "-command $PSScriptRoot\Scripts\UIManager.ps1"
$Shortcut.WorkingDirectory = "$PSScriptRoot\Scripts"
$Shortcut.Save()
}

$action = Read-Host "Enter a name for the config files
They are stored in \Scripts\Configs
I recommend 'ark1,ark2,ark3...'
    
"
if ($action -ne "")
{
    $a = $action.split(',')
    foreach ($create in $a)
    {
        if (-NOT (Test-Path $PSScriptRoot\Scripts\Configs\$create.ini)) 
        {
            "SteamLogin=anonymous anonymous
Branch=376030
ServerIP=ServerIP
ArkServerPath=PathToServerFolder
mcrconExec=PathTomcrconExec
SteamCMDexe=PathToSteamCMDFolder
SteamAPIKey=YourSteamAPIKey
BackupFolder=YourBackupFolder
QueryPort=27015
Players=64
Rcon=true
RconPort=32337
RconPassword=YOURRCONPASSWORD
Port=7777
Map=TheIsland
Mods=731604991,784917362,538827119,924619115,1368937049,569786012,1373937944
StartParameter=PreventDownloadSurvivors=False?PreventDownloadItems=False?PreventDownloadDinos=False?PreventUploadSurvivors=False?PreventUploadItems=False?PreventUploadDinos=False?noTributeDownloads=false?listen -UseBattlEye -ServerGameLog -AutoDestroyStructures -automanagedmods -StasisKeepControllers -NoTransferFromFiltering -clusterid=YOURCLUSTERID -ClusterDirOverride=PATHTOCLUSTERFOLDER" | Out-File $PSScriptRoot\Scripts\Configs\$create.ini
        }
    }
    $temp = $action -replace "," , ".ini,"
    $server = "$temp.ini"
    CreateShortcuts -server "$server"
}