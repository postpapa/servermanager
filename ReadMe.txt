1. Install mcrcon https://bukkit.org/threads/admin-rcon-mcrcon-remote-connection-client-for-minecraft-servers.70910/
2. obtain SteamAPIkey https://steamcommunity.com/dev?l=english
3. run Init.ps1 via powershell
	> enter the server names 
		> if you dont use ark1,ark2,ark3.....  you'll need to edit the scripts in /ServerManager/Scripts which end with ....UI.ps1 
			> there you should find these lines:
				if ($temp -ne "")
				{
					if ($temp -eq "all" )
					{
						Set-Variable -name "server" -value ark1,ark2,ark3
						
			> you'll need to replace "ark1,ark2,ark3" with the names you entered there
			> if you used "ark1,ark2,ark3,ark4" you'll need to add ",ark4" if you want to execute the command on these 4 servers with the "all" argument.
4. edit the .ini files in /ServerManager/Scripts/Configs
	> DO NOT RENAME THEM
5. from now you could use "Manager.ink" to manage your servers manual
6. or create tasks via windows task planner
	> therefor you could use the .ink's in /ServerManager/TaskPlanner
	> they run the commands for all instances you entered in step 3.
7. you have to run your servers with "-automanagedmods"
8. If you want to change the Warning interval adjust it in the lib.ps1 `Function Warning`. default is 10 minutes