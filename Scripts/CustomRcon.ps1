﻿do
{
    $command = Read-Host 'Enter "quit" to exit
Enter Rcon-Command'
    if ( $command -eq "quit" )
    {
        return
    }
    $temp = Read-Host "

'ark1' for a single server 
'ark1,ark2' for multiple servers 
or 'all' for all servers
    
Enter Server"
    if ($temp -ne "")
    {
        if ($temp -eq "all" )
        {
            Set-Variable -name "server" -value ark1,ark2,ark3
        }
        else 
        {
            Set-Variable -name "server" -value $temp
        }
        $Configs = $server.split(',') 
        foreach ($conf in $Configs)
        {
            $exec = "$conf.ini"
            . "$PSScriptRoot\lib.ps1"
            $ErrorActionPreference= 'silentlycontinue'
            $Process = Get-Process ShooterGameServer  | Where-Object {$_.Path -match $conf}
            $ErrorActionPreference= 'continue'
            if ($Process -ne $null)
            { 
                CustomRcon -config "$exec" -command "$command"
            }
            else
            {
                "Server $conf is not running"
            }
        }
    }
} while ( $command -ne "quit" )
