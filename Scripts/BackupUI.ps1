﻿$temp = Read-Host "This will Backup the Server-Files!

'ark1' for a single server 
'ark1,ark2' for multiple servers 
or 'all' for all servers
    
Enter Server"
if ($temp -ne "")
{
    if ($temp -eq "all" )
    {
        Set-Variable -name "server" -value ark1,ark2,ark3,ark4
    }
    else 
    {
        Set-Variable -name "server" -value $temp
    }
    $Configs = $server.split(',') 
    foreach ($conf in $Configs)
    {
        $exec = "$conf.ini"
        . "$PSScriptRoot\lib.ps1"
        $configfile = Parse-IniFile("Configs\$exec")
        Backup -config "$exec"
        "Backup for $conf created"
    }
}
"Everything done, this window will close in 5 secs"
Start-Sleep 5