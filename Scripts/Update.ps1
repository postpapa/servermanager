﻿$Configs = $args.split(',')
foreach ($conf in $Configs)
{
    $time = Get-Date
    $server = $conf.trim(".ini")

#----------------------- Get ServerConfigs -----------------------

    . "$PSScriptRoot\lib.ps1"
    $opts = @("SteamLogin","Branch","ServerIP","ArkServerPath","SteamCMDexe","SteamAPIKey","QueryPort","Players","Rcon","RconPort","RconPassword","Port","Map","Mods","StartParameter")

    $configfile = Parse-IniFile("Configs\$conf");

    foreach($opt in $opts) 
    { 
        Set-Variable $opt ($configfile["NO_SECTION"]["$opt"])
    }
    $ErrorActionPreference= 'silentlycontinue'
    $Process = Get-Process ShooterGameServer  | Where-Object {$_.Path -match $server}
    $ErrorActionPreference= 'continue'
    if ($Process -ne $null)
    {
        
#----------------------- Check for ARK Updates -----------------------

        "$time $server Checking for Updates" | out-file  Logs\Awesome.log -Append
        Start-Transcript -path Logs\DONTDELETE\arkupdate.log
        & $SteamCMDexe\steamcmd.exe +login anonymous +force_install_dir $ArkServerPath +app_update $Branch +exit | Out-Default
        stop-transcript
        $file = Get-Content Logs\DONTDELETE\arkupdate.log
        $ArkUpdate = $file | %{$_ -match "already up to date"}
        if ($ArkUpdate -contains $true)
        {
            "No ARK Update"
            $time = Get-Date
            $server = $conf.trim(".ini")
            "$time $server ARK is Up-To-Date" | out-file  Logs\Awesome.log -Append
        }
        else
        {
            "ARK Update Needed"
            $time = Get-Date
            $server = $conf.trim(".ini")
            "$time $server ARK Update: Restart initialized" | out-file  Logs\Awesome.log -Append
            . "$PSScriptRoot\lib.ps1"
            Restart -config "$conf" -reason "ARK Update: Restart server"
        }
        rm Logs\DONTDELETE\arkupdate.log

#----------------------- Check for Mod Updates -----------------------

        $url = "https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1/";
        $ids = $configfile["NO_SECTION"]["Mods"].split(',')
        $data = @{
            'format' = 'json'
            'key' = '{0}' -f $SteamAPIKey
        }

        $data.itemcount = $ids.length;
# does not work... gets cast to one string
#$data.publishedfileids = $ids;

        for($i = 0; $i -lt $ids.Length; $i++)
        {
            $data.Add('publishedfileids[' + $i + ']', $ids[$i]);
        }

        $response = Invoke-RestMethod -Uri $url -Method Post -Body $data -ContentType "application/x-www-form-urlencoded";

        $file_details = $response.response.publishedfiledetails;

        $print = $file_details | Format-Table -AutoSize -Property @("publishedfileid", "time_updated");
        if (!(Test-Path "Logs\DONTDELETE\$conf-Mods.txt"))
        {
            $print | Out-File Logs\DONTDELETE\$conf-Mods.txt
        }
        else
        {
            $print | Out-File Logs\DONTDELETE\$conf-Mods-temp.txt
            $old = Get-Content Logs\DONTDELETE\$conf-Mods.txt
            $new = Get-Content Logs\DONTDELETE\$conf-Mods-temp.txt
            Compare-Object $old $new | select-string -pattern '=>' -notmatch | Out-File Logs\DONTDELETE\$conf-diff.txt
            foreach ($check in ($ids)) 
            {
                $file = Get-Content Logs\DONTDELETE\$conf-diff.txt
                $containsIDs = $file | %{$_ -match "$check"}
                if ($containsIDs -contains $true)
                {
                    "Mod $check Update Needed"
                }
                else
                {
                    "Mod $check Up-to-Date"
                }
            }
            $file = "Logs\DONTDELETE\$conf-diff.txt"
            if (Test-Path $file) 
            {
                if ((Get-Item $file).length -gt 0kb)
                {
                    $time = Get-Date
                    $server = $conf.trim(".ini")
                    "$time $server Mod Update: Restart initialized" | out-file  Logs\Awesome.log -Append
                    . "$PSScriptRoot\lib.ps1"
                    Restart -config "$conf" -reason "Mod Update: Restart server"
                }
            else 
            {
                "All Mods Up-To-Date"
                $time = Get-Date
                $server = $conf.trim(".ini")
                "$time $server Mods are Up-To-Date" | out-file  Logs\Awesome.log -Append
            }
            }
            $print | Out-File Logs\DONTDELETE\$conf-Mods.txt
            rm Logs\DONTDELETE\$conf-diff.txt
            rm Logs\DONTDELETE\$conf-Mods-temp.txt
        }
    }
}
"Everything done, this window will close in 5 secs"
Start-Sleep 5