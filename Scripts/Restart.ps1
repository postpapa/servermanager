﻿$Configs = $args.split(',')
foreach ($conf in $Configs)
{
    . "$PSScriptRoot\lib.ps1"
    $server = $conf.trim(".ini")
    $ErrorActionPreference= 'silentlycontinue'
    $Process = Get-Process ShooterGameServer  | Where-Object {$_.Path -match $server}
    $ErrorActionPreference= 'continue'
    if ($Process -ne $null)
    {
        $time = Get-Date
        $server = $conf.trim(".ini")
        "$time $server Restart initialized" | out-file  Logs\Awesome.log -Append
        Restart -ArkServerPath -config "$conf" -reason "Restart server"
        Start-Sleep 60
    }
}