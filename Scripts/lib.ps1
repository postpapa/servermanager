﻿Function Parse-IniFile ($file) {
  $ini = @{}

  # Create a default section if none exist in the file. Like a java prop file.
  $section = "NO_SECTION"
  $ini[$section] = @{}

  switch -regex -file $file {
    "^\[(.+)\]$" {
      $section = $matches[1].Trim()
      $ini[$section] = @{}
    }
    "^\s*([^#].+?)\s*=\s*(.*)" {
      $name,$value = $matches[1..2]
      # skip comments that start with semicolon:
      if (!($name.StartsWith(";"))) {
        $ini[$section][$name] = $value.Trim()
      }
    }
  }
  
  return $ini
}

Function Restart ($config, $reason)
{
    $opts = @("SteamLogin","Branch","ServerIP","ArkServerPath","mcrconExec","SteamCMDexe","QueryPort","Players","Rcon","RconPort","RconPassword","Port","Map","Mods","StartParameter")
    $configfile = Parse-IniFile("Configs\$config");
    foreach($opt in $opts) 
    { 
        Set-Variable $opt ($configfile["NO_SECTION"]["$opt"])
    }
    Warning -config "$config" -reason "$reason"
    StopServer -config "$config" -reason "$reason"
    Start-Sleep -s 5
    StartServer -config "$config"
}

Function StartServer ($config)
{
    $time = Get-Date
    $server = $config.trim(".ini")
    "$time $server Starting Server" | out-file  Logs\Awesome.log -Append
    $opts = @("SteamLogin","Branch","ServerIP","ArkServerPath","mcrconExec","SteamCMDexe","QueryPort","Players","Rcon","RconPort","RconPassword","Port","Map","Mods","StartParameter")
    $configfile = Parse-IniFile("Configs\$config");
    foreach($opt in $opts) 
    { 
        Set-Variable $opt ($configfile["NO_SECTION"]["$opt"])
    }
    $StartParams = "${Map}?QueryPort=${QueryPort}?Port=${Port}?RCONEnabled=${Rcon}?RCONPort=${RconPort}?MaxPlayers=${Players}?GameModIds=${Mods}?${StartParameter}"
    & $SteamCMDexe"\steamcmd.exe" +login anonymous +force_install_dir $ArkServerPath +app_update $Branch +exit
    & $ArkServerPath\ShooterGame\Binaries\Win64\ShooterGameServer.exe $StartParams
    UpdateModTime -config "$config"
}

Function StopServer ($config, $reason)
{
    $opts = @("SteamLogin","Branch","ServerIP","ArkServerPath","mcrconExec","SteamCMDexe","QueryPort","Players","Rcon","RconPort","RconPassword","Port","Map","Mods","StartParameter")
    $configfile = Parse-IniFile("Configs\$config");
    foreach($opt in $opts) 
    { 
        Set-Variable $opt ($configfile["NO_SECTION"]["$opt"])
    }
    $server = $config.trim(".ini")
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword "doexit"
    $time =	Get-Date
    Write-Host $time $server $reason
    "$time $server $reason" | out-file  Logs\Awesome.log -Append
}

Function Warning ($config, $reason)
{
    $global:notempty = "1"
    $opts = @("SteamLogin","Branch","ServerIP","ArkServerPath","mcrconExec","SteamCMDexe","QueryPort","Players","Rcon","RconPort","RconPassword","Port","Map","Mods","StartParameter")
    $configfile = Parse-IniFile("Configs\$config");
    $server = $config.trim(".ini")
    foreach($opt in $opts) 
    { 
        Set-Variable $opt ($configfile["NO_SECTION"]["$opt"])
    }
    $10minWarning ="broadcast $reason in 10 minutes!"
    $5minWarning ="broadcast $reason in 5 minutes!"
    $1minWarning ="broadcast $reason in 1 minute!"
    $30secWarning ="broadcast $reason in 30 secs!"
    $10secWarning ="broadcast $reason in 10 secs!"
    Playercount -config "$config"
    if ($notempty -eq "0")
    {
    return
    }
    #Server Message 10 minute warning	
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword $10minWarning
    $time =	Get-Date
    Write-Host $time $server $reason in 10 min 
    Start-Sleep -s 60
    Playercount -config "$config"
    if ($notempty -eq "0")
    {
    return
    }
    Start-Sleep -s 60
    Playercount -config "$config"
    if ($notempty -eq "0")
    {
    return
    }
    Start-Sleep -s 60
    Playercount -config "$config"
    if ($notempty -eq "0")
    {
    return
    }
    Start-Sleep -s 60
    Playercount -config "$config"
    if ($notempty -eq "0")
    {
    return
    }
    Start-Sleep -s 60
    Playercount -config "$config"
    if ($notempty -eq "0")
    {
    return
    }
    #Server Message 5 minute warning			
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword $5minWarning	
    $time =	Get-Date
    Write-Host $time $server $reason in 5 min
    Start-Sleep -s 60
    Playercount -config "$config"
    if ($notempty -eq "0")
    {
    return
    }
    Start-Sleep -s 60
    Playercount -config "$config"
    if ($notempty -eq "0")
    {
    return
    }
    Start-Sleep -s 60
    Playercount -config "$config"
    if ($notempty -eq "0")
    {
    return
    }
    Start-Sleep -s 60
    Playercount -config "$config"
    if ($notempty -eq "0")
    {
    return
    }
    #Server Message 1 minute warning
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword $1minWarning			
    $time =	Get-Date
    Write-Host $time $server $reason in 1 min
    Start-Sleep -s 30
    #Server Message 30 secs warning
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword $30secWarning			
    $time =	Get-Date
    Write-Host $time $server $reason in 30 sec
    Start-Sleep -s 20
    #Server Message 10 secs warning
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword $10secWarning			
    $time =	Get-Date
    Write-Host $time $server $reason in 10 sec
    Start-Sleep -s 10
}

Function Playercount ($config)
{
    $opts = @("SteamLogin","Branch","ServerIP","ArkServerPath","mcrconExec","SteamCMDexe","QueryPort","Players","Rcon","RconPort","RconPassword","Port","Map","Mods","StartParameter")
        $configfile = Parse-IniFile("Configs\$config");
        foreach($opt in $opts) 
        { 
            Set-Variable $opt ($configfile["NO_SECTION"]["$opt"])
        }
    $output = & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword "listplayers"
    if ($output -match "No Players Connected")
    {
        "noone connected"
        $global:notempty = "0"
    }
    else
    {
        $pc = ($output.Count-2)
        "$pc Players connected"
    }
}

Function Saveworld ($config)
{
    $opts = @("SteamLogin","Branch","ServerIP","ArkServerPath","mcrconExec","SteamCMDexe","QueryPort","Players","Rcon","RconPort","RconPassword","Port","Map","Mods","StartParameter")
    $configfile = Parse-IniFile("Configs\$config");
    foreach($opt in $opts) 
    { 
        Set-Variable $opt ($configfile["NO_SECTION"]["$opt"])
    }
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword "saveworld"
    $server = $config.trim(".ini")
    $time =	Get-Date
    Write-Host $time  $server world saving Server
    "$time $server World saved" | out-file  Logs\Awesome.log -Append
}

Function Backup ($config)
{        
    $configfile = Parse-IniFile("Configs\$config");
    $ArkServerPath = $configfile["NO_SECTION"]["ArkServerPath"]
    $BackupFolder = $configfile["NO_SECTION"]["BackupFolder"]
    $time = Get-Date
    $time2 = "{0:yyyy-MM-dd HH.mm.ss}" -f (get-date)
    $server = $config.trim(".ini")
    $source = "$ArkServerPath\ShooterGame\Saved\SavedArks"
    $destination = "$BackupFolder\${time2}-${server}.zip"
    If(!(test-path $BackupFolder))
    {
        mkdir $BackupFolder
    }
    Add-Type -assembly "system.io.compression.filesystem"
    [io.compression.zipfile]::CreateFromDirectory($source,$destination)
    "$time $server Backup Created" | out-file  Logs\Awesome.log -Append
}

Function Dinowipe ($config)
{
    $opts = @("SteamLogin","Branch","ServerIP","ArkServerPath","mcrconExec","SteamCMDexe","QueryPort","Players","Rcon","RconPort","RconPassword","Port","Map","Mods","StartParameter")
    $configfile = Parse-IniFile("Configs\$config");
    foreach($opt in $opts) 
    { 
        Set-Variable $opt ($configfile["NO_SECTION"]["$opt"])
    }
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword "broadcast Execute Wilddino wipe now"
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword "destroyall BeeHive_c"
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword "destroywilddinos"
    $time =	Get-Date
    $server = $config.trim(".ini")
    Write-Host $time $server Wilddinos and beehives destroyed
    "$time $server Wilddinos and beehives destroyed" | out-file  Logs\Awesome.log -Append
}

Function CustomRcon ($config, $command)
{
    $opts = @("SteamLogin","Branch","ServerIP","ArkServerPath","mcrconExec","SteamCMDexe","QueryPort","Players","Rcon","RconPort","RconPassword","Port","Map","Mods","StartParameter")
    $configfile = Parse-IniFile("Configs\$config");
    foreach($opt in $opts) 
    { 
        Set-Variable $opt ($configfile["NO_SECTION"]["$opt"])
    }
    & $mcrconExec -c -H "$ServerIP" -P $RconPort -p $RconPassword "$command"
    $time =	Get-Date
    $server = $config.trim(".ini")
    Write-Host $time $server $command executed
    "$time $server $command executed" | out-file  Logs\Awesome.log -Append
}

Function UpdateModTime ($config)
{
    $opts = @("SteamLogin","Branch","ServerIP","ArkServerPath","mcrconExec","SteamCMDexe","QueryPort","Players","Rcon","RconPort","RconPassword","Port","Map","Mods","StartParameter")
    $configfile = Parse-IniFile("Configs\$config");
    foreach($opt in $opts) 
    { 
        Set-Variable $opt ($configfile["NO_SECTION"]["$opt"])
    }
    $url = "https://api.steampowered.com/ISteamRemoteStorage/GetPublishedFileDetails/v1/";
    $ids = $configfile["NO_SECTION"]["Mods"].split(',')
    $data = @{
        'format' = 'json'
        'key' = '{0}' -f $SteamAPIKey
    }
    $data.itemcount = $ids.length;
    for($i = 0; $i -lt $ids.Length; $i++)
    {
        $data.Add('publishedfileids[' + $i + ']', $ids[$i]);
    }
    $response = Invoke-RestMethod -Uri $url -Method Post -Body $data -ContentType "application/x-www-form-urlencoded";
    $file_details = $response.response.publishedfiledetails;
    $print = $file_details | Format-Table -AutoSize -Property @("publishedfileid", "time_updated");
    $print | Out-File Logs\DONTDELETE\$config-Mods.txt
}