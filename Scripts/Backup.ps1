﻿$Configs = $args.split(',')
foreach ($conf in $Configs)
{
    $time = Get-Date
    $server = $conf.trim(".ini")
    "$time $server Backup initialized" | out-file  Logs\Awesome.log -Append
    . "$PSScriptRoot\lib.ps1"
    Backup -config "$conf"
}