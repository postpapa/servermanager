﻿$Configs = $args.split(',')
foreach ($conf in $Configs)
{
    . "$PSScriptRoot\lib.ps1"
    $server = $conf.trim(".ini")
    $ErrorActionPreference= 'silentlycontinue'
    $Process = Get-Process ShooterGameServer  | Where-Object {$_.Path -match $server}
    $ErrorActionPreference= 'continue'
    if ($Process -ne $null)
    {
        Warning -config "$conf" -reason "Shutdown server"
        StopServer -config "$conf" -reason "Shutdown server"
    }
}