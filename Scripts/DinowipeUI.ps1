﻿$temp = Read-Host "This will wipe all wild dinos and beehives!

'ark1' for a single server 
'ark1,ark2' for multiple servers 
or 'all' for all servers
    
Enter Server"
if ($temp -ne "")
{
    if ($temp -eq "all" )
    {
        Set-Variable -name "server" -value ark1,ark2,ark3
    }
    else 
    {
        Set-Variable -name "server" -value $temp
    }
    $Configs = $server.split(',') 
    foreach ($conf in $Configs)
    {
        $exec = "$conf.ini"
        . "$PSScriptRoot\lib.ps1"
        $ErrorActionPreference= 'silentlycontinue'
        $Process = Get-Process ShooterGameServer  | Where-Object {$_.Path -match $conf}
        $ErrorActionPreference= 'continue'
        if ($Process -ne $null)
        { 
            Dinowipe -config "$exec"
        }
        else
        {
            "Server $conf is not running"
        }
    }
}
"Everything done, this window will close in 5 secs"
Start-Sleep 5