﻿do
{
    $action = Read-Host "Enter an action to perform
    1) Start Server
    2) Restart Server
    3) Stop Server
    4) Stop Server without Warning
    5) Saveworld
    6) Backup Server
    7) Wild Dino Wipe
    8) Rcon commands
    9) Update check
    'quit' to close this window
    
    "

    switch( $action )
    {
        1 
        {
            start powershell "$PSScriptRoot\StartUI.ps1"
        }
        2
        {
            start powershell "$PSScriptRoot\RestartUI.ps1"
        }
        3
        {
            start powershell "$PSScriptRoot\StopUI.ps1"
        }
        4
        {
            start powershell "$PSScriptRoot\StopWITHOUTWarningUI.ps1"
        }
        5
        {
            start powershell "$PSScriptRoot\SaveworldUI.ps1"
        }
        6
        {
            start powershell "$PSScriptRoot\BackupUI.ps1"
        }
        7
        {
            start powershell "$PSScriptRoot\DinowipeUI.ps1"
        }
        8
        {
            start powershell "$PSScriptRoot\CustomRcon.ps1"
        }
        9
        {
            start powershell "$PSScriptRoot\Update.ps1 ark1.ini,ark2.ini,ark3.ini,ark4.ini"
        }
    }
    Clear-Host
} while( $action -ne "quit")